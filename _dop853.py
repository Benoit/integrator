from ._dop import dop853 as dop8f
from ._dop import contd8
import numpy as np

def dop853(f, t0, y0, tmax, atol, rtol, solout=None, dens=False, t_eval=[], h_init=0):
    """y' = f(t, y) from y0 at t0 to t1
    t0: initial time
    y0: initial stat (real scalar or vector)
    t1: final time
    atol: absolute tolerance
    rtol: relative tolerance
    solout(told, t, y, con): called at each succesfull step. 
            Can return a negative int to stop integration
    dens: if True return 'con' in solout to use for dens output (dens8)
    t_eval: time to eval function (mutually exclusive with solout and dens)
    h_init: initial step, 0 use adaptative initial step
    return: tmax, y(tmax) or 
            [(ta, ya), (tb, yb), ...] when t_eval=[ta, tb, ...] 
    """
    y0  = np.reshape(y0, -1)
    n = len(y0)
    iwork = np.zeros((21 + n,), np.int32)
    iwork[2] = -1  # no print allowed
    if dens or len(t_eval):
        iwork[4] = n
    work = np.zeros((11 * n + 8 * iwork[4] + 21),)
    work[6] = h_init
    iout = 0
    if solout is not None:
        iout = 1
        if dens:
            iout = 2
    else:
        solout = lambda *x: None
    
    if len(t_eval):
        t_eval = sorted(t_eval) 
        iout = 2
        res = []
        def solout(told, t1, y, con):            
            if told != t1:
                j = np.searchsorted(t_eval, t1, "right")
                for t in t_eval[len(res): j]:
                    res.append((t, dens8(t, told, t1, con)))
    a = dop8f(f, t0, y0, tmax, rtol, atol, solout, iout, work, iwork, ())
    if len(t_eval):
        return res
    else:
        return a

def dens8(t, t0, t1, con):
    """t0 <= t <= t1"""
    return contd8(t0, t1, t, con)[1]
    