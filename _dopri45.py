import numpy as np
from math import sqrt
import warnings


class dopri45:
    """Inspired by
    https://github.com/scipy/scipy/blob/master/scipy/integrate/dop/dopri5.f

    Example:
    >>> r = Dopri45(atol, rtol)
    >>> r.run(*args)
    """

    C2 = 0.2
    C3 = 0.3
    C4 = 0.8
    C5 = 8/9
    A21 = 0.2
    A31 = 3/40
    A32 = 9/40
    A41 = 44/45
    A42 = -56/15
    A43 = 32/9
    A51 = 19372/6561
    A52 = -25360/2187
    A53 = 64448/6561
    A54 = -212/729
    A61 = 9017/3168
    A62 = -355/33
    A63 = 46732/5247
    A64 = 49/176
    A65 = -5103/18656
    A71 = 35/384
    A73 = 500/1113
    A74 = 125/192
    A75 = -2187/6784
    A76 = 11/84
    E1 = 71/57600
    E3 = -71/16695
    E4 = 71/1920
    E5 = -17253/339200
    E6 = 22/525
    E7 = -1/40

    FACC1 = 1/0.2
    FACC2 = 1/10

    FACOLD = 1e-4

    SAFE = 0.9
    BETA = 0.04
    EXPO1 = 0.2 - BETA * 0.75

    NMAX = 100000
    ROUND = 2.3e-16

    def __init__(self, atol=1e-9, rtol=1e-9):
        self.ATOL = atol
        self.RTOL = rtol
        self.IASTI = 0  # Stiffness detector
        self.HLAMB = 0
        self.NACCPT = 0
        self.NSTEP = 0
        self.NREJCT = 0
        self.NSTIFF = 1000
        self.NONSTI = 0

    def step(self, f, X, Y, H, K1):

        if self.NSTEP >= self.NMAX:
            warnings.warn('More than Nmax = %d steps are needed' % self.NMAX)
            return False, Y, X, H, K1
        if 0.1 * abs(H) <= abs(X) * self.ROUND:
            warnings.warn('Step size too small, H = %d' % H)
            return False, Y, X, H, K1

        self.NSTEP += 1
        # The first 6 stages
        Y1 = Y + H * self.A21 * K1
        K2 = f(X + self.C2 * H, Y1)

        Y1 = Y + H * (self.A31 * K1 + self.A32 * K2)
        K3 = f(X + self.C3 * H, Y1)

        Y1 = Y + H * (self.A41 * K1 + self.A42 * K2 + self.A43 * K3)
        K4 = f(X + self.C4 * H, Y1)

        Y1 = Y + H * (self.A51 * K1 + self.A52 * K2 + self.A53 * K3
                      + self.A54 * K4)
        K5 = f(X + self.C5 * H, Y1)

        XPH = X + H
        YSTI = Y + H * (self.A61 * K1 + self.A62 * K2 + self.A63 * K3
                        + self.A64 * K4 + self.A65 * K5)
        K6 = f(XPH, YSTI)

        # Last stage
        Y1 = Y + H * (self.A71 * K1 + self.A73 * K3 + self.A74 * K4
                      + self.A75 * K5 + self.A76 * K6)
        K2 = f(XPH, Y1)

        # Error
        K4 = np.abs((self.E1 * K1 + self.E3 * K3 + self.E4 * K4 + self.E5 * K5
                     + self.E6 * K6 + self.E7 * K2) * H)

        # Error estimation
        nY = np.abs(Y)
        nY1 = np.abs(Y1)
        nor = np.maximum(nY, nY1)

        SK = self.ATOL + self.RTOL * nor
        ERR = sqrt(np.sum((K4 / SK)**2)/self.N)

        # Computation of HNEW
        FAC11 = ERR**self.EXPO1
        # Lund-Sabilization
        FAC = FAC11 / self.FACOLD**self.BETA
        # We Require  FAC1 <= HNEW/H <= FAC2
        FAC = max(self.FACC2, min(self.FACC1, FAC / self.SAFE))
        HNEW = H / FAC

        if(ERR <= 1):
            # Step is accepted
            self.FACOLD = max(ERR, 1e-4)
            self.NACCPT = self.NACCPT + 1
            # Stiffness detection
            if (self.NACCPT % self.NSTIFF) == 0 or self.IASTI > 0:
                STNUM = np.sum(np.abs(K2 - K6)**2)
                STDEN = np.sum(np.abs(Y1 - YSTI)**2)
                if STDEN > 0:
                    self.HLAMB = H * sqrt(STNUM / STDEN)
                if self.HLAMB > 3.25:
                    self.NONSTI = 0
                    self.IASTI = self.IASTI + 1
                    if self.IASTI == 15:
                        warnings.warn('Exit of dopri at X = %' % X)
                        raise UserWarning("Dopri45")
                else:
                    self.NONSTI += 1
                    if self.NONSTI == 6:
                        self.IASTI = 0
            if self.REJECT:
                HNEW = min(abs(HNEW), abs(H))
                self.REJECT = False
        else:
            # Step is rejected
            HNEW = H / min(self.FACC1, FAC11 / self.SAFE)
            self.REJECT = True
            if self.NACCPT >= 1:
                self.NREJCT = self.NREJCT + 1
            return True, Y, X, HNEW, K1

        HNEW = self.POSNEG * min(abs(HNEW), abs(XPH - self.XMAX))

        return True, Y1, XPH, HNEW, K2

    def init_H(self, f, X, Y, F0):
        # COMPUTATION OF AN INITIAL STEP SIZE GUESS
        SK = self.ATOL + self.RTOL * np.sum(np.abs(Y))
        DNF = np.sum(np.abs(F0 / SK)**2)
        DNY = np.sum(np.abs(Y / SK)**2)
        if (DNF <= 1e-10 or DNY <= 1e-10):
            H = 1e-6
        else:
            H = sqrt(DNY / DNF) * 0.01
        H = min(H, abs(X - self.XMAX))
        H = self.POSNEG * H

        # PERFORM AN EXPLICIT EULER STEP
        Y1 = Y + H * F0
        F1 = f(X + H, Y1)

        # ESTIMATE THE SECOND DERIVATIVE OF THE SOLUTION
        SK = self.ATOL + self.RTOL * np.abs(Y)
        DER2 = sqrt(np.sum(np.abs((F1 - F0) / SK)**2)) / H

        # STEP SIZE IS COMPUTED SUCH THAT
        # H**5 * MAX ( NORM (F0), NORM (DER2)) = 0.01
        DER12 = max(abs(DER2), sqrt(DNF))
        if (DER12 <= 1e-15):
            H1 = max(1e-6, abs(H)*1e-3)
        else:
            H1 = (0.01 / DER12)**(1 / 5)
        H = min(100 * abs(H), H1, abs(X - self.XMAX))

        self.REJECT = False

        return self.POSNEG * H

    def run(self, f, y0, t0, tmax, solout=None):
        """
        Solve dy/dt = f(t, y) with y(t0) = Y0.
        Work for complex value and multiple dimensional array.

        Arguments:
        f(t, y): return dy/dt(t)
        this function is call at each sucessful time step
        y0: initial value
        t0: initial time
        tmax: final time
        solout(t, y, h): must return next time step.
        Called at each succesfull step. h: next time step.
        return:
        y(tmax)
        """
        X = t0
        Y = y0
        self.XMAX = tmax
        self.N = np.prod(np.shape(Y))

        if abs(X - self.XMAX) <= self.ROUND:
            return Y

        self.POSNEG = np.sign(self.XMAX - X)

        K1 = f(X, Y)
        H = self.init_H(f, X, Y, K1)
        if solout:
            H = solout(X, Y, H)

        loop = True
        while (X - self.XMAX) * self.POSNEG <= -self.ROUND and loop:
            loop, Y, X, H, K1 = self.step(f, X, Y, H, K1)
            if solout and not self.REJECT:
                H = solout(X, Y, H)

        return Y
